from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://localhost/famlicious'
db = SQLAlchemy(app)

STRING = db.String(255)

class Locations(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(STRING)

    def __str__(self):
        return title

class Persons(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(STRING)
    last_name = db.Column(STRING)
    status = db.Column(STRING)
    phone = db.Column(STRING)
    location_id = db.Column(db.Integer, db.ForeignKey('Locations.id'))
    location = db.relationship('Locations', backref=db.backref('persons', lazy='dynamic'))
    sex = db.Column(STRING)
    date_of_birth = db.Column(db.DateTime)
    date_of_death = db.Column(db.DateTime)
    chat_user_id = db.Column(db.Integer)
    avatar_file_name = db.Column(STRING)
    avatar_content_type = db.Column(STRING)
    avatar_file_size = db.Column(db.Integer)
    avatar_uploaded_at = db.Column(db.DateTime)
    created_at = db.Column(db.DateTime)
    updated_at = db.Column(db.DateTime)
    deleted_at = db.Column(db.DateTime)

    email = db.Column(STRING)
    encrypted_password = db.Column(STRING)
    digits_id = db.Column(db.Integer)
    reset_password_token = db.Column(STRING)
    reset_password_sent_at = db.Column(db.DateTime)
    remember_created_at = db.Column(db.DateTime)
    signin_count = db.Column(db.Integer, default=0)
    current_signin_at = db.Column(db.DateTime)
    last_signin_at = db.Column(db.DateTime)
    current_signin_ip = db.Column(STRING)
    last_signin_ip = db.Column(STRING)
    token = db.Column(STRING)

class Users(Person):
    def __getattribute__(self, name):
        return Person.name
