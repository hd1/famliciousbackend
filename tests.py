import unittest

from flask import Flask
from flask.ext.testing import LiveServerTestCase 
import requests


# Testing with LiveServer
class Tests(LiveServerTestCase):
    # if the create_app is not implemented NotImplementedError will be raised
    def create_app(self):
        app = Flask(__name__)
        app.config['TESTING'] = True
        return app 

    def test_recreate_session(self):
        reply = requests.get('http://localhost:5000/api/v1/auth/recreate_session').json()
        EXPECTED_KEYS = ['id', 'chat_id', 'first_name', 'last_name', 'avatar', 'email', 'status', 'location', 'sex', 'date_of_birth','date_of_death','chat_token','chat_token_expires_in']
        self.assertTrue(all(reply.has_key(k) for k in EXPECTED_KEYS))

if __name__ == '__main__':
    unittest.main()
